# Mikko Test
## Introduction
<p>For this test you are required to write an application, which we will analyse to evaluate your skills.
The test is designed in such a way that we can derive a number of qualities from your code and
project.</p>
<p>
We want you to use what you consider to be the best practices and treat this application as you
would treat any other project or deliverable for production purposes. This will allow us to get an
idea of your coding skills and the standards you use.</p>
<p>We ask you to send your work ASAP, but please tell us if that doesn't work for you for some reason.
We rather receive a solution that you consider as finished than something half done. The actual
amount of time you spend on it, is completely up to you. If you need frameworks, libraries,
databases, ... to write the application, please mention them in the documentation for your
application.</p>
<p>Once finished, send the resulting code to your recruitment contact.</p>

## Requirements
<p>You are required to create a utility in PHP to help us determine the dates we need to pay salaries to
the sales department.</p>

We handle our sales payroll as follows:
* Sales staff get a regular monthly fixed base salary. The salary for a month is paid on the last
day of the month, unless that day is a weekend day (Saturday, Sunday). In that case, the
salary is paid on the Friday before said weekend.
* Sales staff get a monthly bonus. On the 15th of every month the bonus is paid for the
previous month, unless that day is a weekend day (Saturday, Sunday). In that case, the bonus
is paid on the first Wednesday after the 15th.
* The application needs to be (at the least) able to output the results in a CSV file. The file
should contain the results for the whole year. The file should contain 3 columns: month, the
salary payment date for that month and the bonus payment date for the bonus of that month.
* The user needs to be able to input the year they want to receive the dates for.

