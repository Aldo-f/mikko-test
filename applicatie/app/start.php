<?php

namespace Mikko;

use DateTime;


if ($_POST) {

    if (isset($_POST['year'])) {
        $year = (int) $_POST['year'];

        if ($year <= 9998 && $year >= 100) {
            $data = (object) array('months' => [], 'salary' => [], 'bonus' => []);

            months($data);
            salary($data, $year);
            bonus($data, $year);

            generateCsv($data, $year);
        } else {
            //invalid 
            $invalid = true;
        }
    };
}
?>

<?php
if (isset($invalid) && $invalid == true) { ?>
<div class="alert alert-warning alert-dismissible fade show" role="alert">
  <strong>Verkeerd jaar!</strong> Het jaartal is minimaal 100 en maximaal 9998
</div>
<?php }
?>

<form action="" method="post" class="">
  <div class="form-group form-inline">
    <label class="mr-4" for="year">Jaar</label>
    <input class="form-control " name="year" type="number" <?php inputValue(); ?>>
    <button type="submit" class="btn btn-primary">Verzend</button>
  </div>
  <small class="form-text text-muted">Van welk jaar wil je de gegevens opvragen?</small>
</form>

<?php
function inputValue()
{

    if (isset($_POST['year'])) {
        $value = $_POST['year'];
    } else {
        $value = date("Y");
    }
    echo ("value=$value");
}

function months(&$data)
{
    for ($i = 1; $i <= 12; $i++) {
        array_push($data->months, date('F', mktime(0, 0, 0, $i, 10)));
    }
}

function salary(&$data, $year)
{
    // last day of month (if weekday, else previous friday end of week)
    for ($i = 1; $i <= 12; $i++) {
        $firstDay = $year  . "-" . (string) $i . "-01";
        $date = new DateTime($firstDay);
        $salaryDay = $date->format('Y-m-t');

        if (checkIsWeekend($salaryDay)) {
            $salaryDay = date('Y-m-d', strtotime('previous friday', strtotime($salaryDay)));
        }
        array_push($data->salary, $salaryDay);
    }
}

function bonus(&$data, $year)
{
    for ($i = 1; $i <= 12; $i++) {
        $date =   strtotime($year  . "-" . (string) $i . "-15");
        $bonusDay = date("Y-m-d", strtotime('+1 months', $date));

        if (checkIsWeekend($bonusDay)) {
            $bonusDay = date('Y-m-d', strtotime('next wednesday', strtotime($bonusDay)));
        }
        array_push($data->bonus, $bonusDay);
    }
}

function checkIsWeekend($date)
{
    $dayOfWeek = date('w', strtotime($date));
    return ($dayOfWeek == 0 || $dayOfWeek == 6);
}

function generateCsv(Object $data, $year = null)
{
    ob_start();
    if ($year) {
        $fh = fopen($year . "-salary-and-bonus-dates.csv", 'w');
    } else {
        $fh = fopen('salary-and-bonus-dates.csv', 'w');
    }

    fputcsv($fh, array_keys((array) $data));

    $data =  transposeData($data);

    foreach ($data as $row) {
        fputcsv($fh, $row);
    }
    ob_flush();
    fclose($fh);

    return ob_get_clean();
}

function transposeData($data)
{
    $retData = array();
    foreach ($data as $row => $columns) {
        foreach ($columns as $row2 => $column2) {
            $retData[$row2][$row] = $column2;
        }
    }
    return $retData;
}
?>