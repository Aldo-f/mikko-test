<?php require 'parts\head.php'; ?>

<body>
  <?php include 'parts\nav.php' ?>

  <main class="container">
    <?php require 'app\start.php'; ?>
  </main>

  <?php include 'parts\scripts.php' ?>
</body>

</html>