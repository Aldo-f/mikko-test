# Analyse

## Gevraagde
* [ ] betaling op laatste dag van de maand, behalve in weekend; dan op de laatste weekdag van de maand. &rarr; laatste weekdag
* [ ] maandelijkse bonus (op fixed salary; van vorige maand) op de 15<sup>de</sup>; behalve waneer deze in het weekend valt. &rarr; 15<sup>de</sup> in weekend &rarr; betaling op eerste woensdag volgend op de 15<sup>de</sup>
* [ ] output data ten minste als een CSV-file
  * [ ] van geheel jaar
  * [ ] 3 columns: maand, datum betaling loon; datum betaling bonus
* [ ] Input van het jaar.


Lijst gegevens: </br>
* jaar input
* per maand
  * datum loon
  * datum bonus

### Gebruikte library
* https://github.com/PHPOffice/PhpSpreadsheet



